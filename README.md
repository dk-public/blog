# blog

## Project Setup
npm install -g @nestjs/cli
nest new api > npm
cd api
npm i --save @nestjs/config
npm install --save @nestjs/typeorm typeorm pg
ConfigModule & TypeOrmModule setup
Created .env file
Removed the .git directory created inside the api project
Disable prettier in api\.eslintrc.js

## User CRUD
git flow init (enter to all options)
git flow feature start User-CRUD
cd api/src
nest generate module user
nest generate service user (move to src/user/service/user.service.ts)
nest generate controller user (move to src/user/controller/user.controller.ts)
change the controler path to users instead of user
update user module to point to the correct paths
create the CRUD functionality user typeorm
